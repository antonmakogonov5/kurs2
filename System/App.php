<?php

namespace System;

class App {
    public static function run() {
        //Разобрать строку запроса
        $path = $_SERVER['REQUEST_URI'];
        //Разбиваем строку запроса
        $paths = explode('/', $path);
        $controller = $paths[1];
        $action = $paths[2];
        //Формируем пространство имен
        $controller = 'Controllers\\' . ucfirst($controller) . 'Controller';
        //Формируем название действия
        $action = 'action' . ucfirst($action);
        //Если класса нет формируем исключение
        if(!class_exists($controller)) {
            throw new \Exception('Controller does not exists');
        }
//        print_r($controller);
        $objController = new $controller;
        //Проверяем наличие метода в контроллере
        if(!method_exists($objController, $action)) {
            throw new \Exception('Action does not exists');
        }
        $objController->$action();
    }
}
